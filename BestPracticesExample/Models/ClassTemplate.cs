﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  UniqSoft.BestPracticesExample.Models
{
    /// <summary>
    /// This ClassTemplate is created as Wallet logic for simple example
    /// </summary>
    public class ClassTemplate
    {
        private string ClassName = "ClassTemplate";

        #region PROPERTIES

        /// <summary>
        /// means this
        /// </summary>
        public int Balance { get; set; }
        /// <summary>
        /// means that.
        /// If parameter is OBVIOUS, xml documentation is not needed (but always nice to have :) )
        /// </summary>
        public int FunBonus { get; set; }

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// reason costruction with no parameters
        /// </summary>
        public ClassTemplate()
        { }

        /// <summary>
        /// reason constructor with parameter
        /// </summary>
        /// <param name="accountId">this parameter means this blablabla</param>
        public ClassTemplate(int accountId)
        { }

        #endregion

        #region INSTANCE METHODS

        /// <summary>
        /// method explanation
        /// </summary>
        /// <param name="amount">parameter explanation</param>
        /// <returns>return explanation</returns>
        public int Withdraw(int amount)
        {
            this.Balance -= amount;

            return this.Balance;
        }

        #region LOGIC SUBDIVISION 1 (i.e. DEPOSITS)

        /// <summary>
        /// method explanation
        /// </summary>
        /// <param name="amount">parameter explanation</param>
        /// <returns>return explanation</returns>
        public int CashDeposit(int amount)
        {
            // do stuff
            this.Balance += amount;

            return this.Balance;
        }

        /// <summary>
        /// method explanation
        /// </summary>
        /// <param name="amount">parameter explanation</param>
        /// <returns>return explanation</returns>
        public int BankAccountDeposit(int amount)
        {
            // do other stuff
            this.Balance += amount;

            return this.Balance;
        }

        #endregion

        #region LOGIC SUBDIVISION 2 (i.e. FUNBONUS MANAGEMENT)

        /// <summary>
        /// method explanation
        /// </summary>
        /// <param name="amount">parameter explanation</param>
        /// <returns>return explanation</returns>
        public int UseFunBonus(int amount)
        {
            this.FunBonus -= amount;

            return this.FunBonus;
        }

        /// <summary>
        /// method explanation
        /// </summary>
        /// <param name="amount">parameter explanation</param>
        /// <returns>return explanation</returns>
        public int AddFunBonus(int amount)
        {
            this.FunBonus += amount;

            return this.FunBonus;
        }
        #endregion

        #endregion

        #region STATIC METHODS

        /// <summary>
        /// method explanation
        /// </summary>
        /// <param name="parentID">parameter explanation</param>
        /// <returns>return explanation</returns>
        public static List<ClassTemplate> GetMyChildrenBalances(int parentID)
        {
            List<ClassTemplate> wallets = null;

            // use logic to retrieve children wallets
            // i.e.
            // wallets = DBManager.GetBalances(parentID);

            return wallets;
        }

        #endregion
    }
}
