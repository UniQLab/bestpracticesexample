﻿using UniqSoft.QFramework.Config;
using UniqSoft.QFramework.DAO;
using UniqSoft.QFramework.Patterns;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using  UniqSoft.BestPracticesExample.Controllers;
using UniqSoft.QFramework.Models;
using System.Configuration;
using  UniqSoft.BestPracticesExample.Settings;

namespace  UniqSoft.BestPracticesExample.Models
{
    public class User
    {
        private string ClassName = "User";

        public int AccountId { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public DateTime BirthDate { get; set; }
        public bool AllowMail { get; set; }
        public DateTime CreationDate { get; set; }

        public BaseResponse Insert()
        {
            string methodName = MethodInfo.GetCurrentMethod().Name;
            BaseResponse ret = null;
            Dictionary<string, object> paramsLog = new Dictionary<string, object>();
         
            try
            {
                // SqlDao method 1: using as parameter connectionstring in web.config / app.config
                using (SqlDao d = new SqlDao(ConfigurationManager.ConnectionStrings["TestConnStr"].ToString()))
                {
                    d.Open();

                    List<SqlParameter> sqlParams = new List<SqlParameter>();

                    #region INPUT PARAMS
                    sqlParams.Add(new SqlParameter("@Name", SqlDbType.VarChar, 50, ParameterDirection.Input, true, 0, 0, "Name", DataRowVersion.Current, this.Name));
                    sqlParams.Add(new SqlParameter("@Age", SqlDbType.Int, 4, ParameterDirection.Input, true, 0, 0, "Age", DataRowVersion.Current, this.Age));
                    sqlParams.Add(new SqlParameter("@BirthDate", SqlDbType.Date, 10, ParameterDirection.Input, true, 0, 0, "BirthDate", DataRowVersion.Current, this.BirthDate));
                    sqlParams.Add(new SqlParameter("@AllowMail", SqlDbType.Bit, 1, ParameterDirection.Input, true, 0, 0, "AllowMail", DataRowVersion.Current, this.AllowMail));
                    sqlParams.Add(new SqlParameter("@CreationDate", SqlDbType.DateTime, 20, ParameterDirection.Input, true, 0, 0, "CreationDate", DataRowVersion.Current, DateTime.Now));
                    #endregion

                    #region OUTPUT PARAMS
                    SqlParameter accountIdParam = new SqlParameter("@AccountId", SqlDbType.Int, 4, ParameterDirection.Output, true, 0, 0, "AccountId", DataRowVersion.Current, 0);
                    SqlParameter errorCodeParam = new SqlParameter("@ErrorCode", SqlDbType.Int, 5, ParameterDirection.ReturnValue, true, 0, 0, "ErrorCode", DataRowVersion.Current, 0);
                    SqlParameter errorMessageParam = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 1000, ParameterDirection.Output, true, 0, 0, "ErrorMessage", DataRowVersion.Current, "");
                    sqlParams.Add(accountIdParam);
                    sqlParams.Add(errorCodeParam);
                    sqlParams.Add(errorMessageParam);
                    #endregion

                    d.ExecuteStoredNonQuery("[Account].[User_Insert]", sqlParams);
                    d.Dispose();

                    if ((int)errorCodeParam.Value == 0)
                        this.AccountId = (int)accountIdParam.Value;

                    ret = new BaseResponse(errorCodeParam.Value, errorMessageParam.Value);
                }
            }
            catch (Exception ex)
            {
                #region LOG EXCEPTION
                paramsLog.Add("addedParam", "I was executing stored XXX");
                Logger.LogError(AppSettings.ApplicationName, this.ClassName, methodName, paramsLog, ex);
                #endregion

                #region MANAGE EXCEPTION
                // as needed

                ret = new BaseResponse(-1, "APPLICATION ERROR");
                #endregion
            }

            return ret;
        }

        public BaseResponse Update()
        {
            string methodName = MethodInfo.GetCurrentMethod().Name;
            BaseResponse ret = null;
            Dictionary<string, object> paramsLog = new Dictionary<string, object>();

            try
            {
                // SqlDao method 2: using as parameter connectionstring in singleton (read only first time used)
                using (SqlDao d = new SqlDao(Singleton<LibParams>.Instance.SqlConnectionString))
                {
                    d.Open();

                    List<SqlParameter> sqlParams = new List<SqlParameter>();

                    #region INPUT PARAMS
                    sqlParams.Add(new SqlParameter("@AccountId", SqlDbType.VarChar, 50, ParameterDirection.Input, false, 0, 0, "AccountId", DataRowVersion.Current, this.AccountId));
                    sqlParams.Add(new SqlParameter("@Name", SqlDbType.VarChar, 50, ParameterDirection.Input, true, 0, 0, "Name", DataRowVersion.Current, this.Name));
                    sqlParams.Add(new SqlParameter("@Age", SqlDbType.Int, 4, ParameterDirection.Input, true, 0, 0, "Age", DataRowVersion.Current, this.Age));
                    sqlParams.Add(new SqlParameter("@BirthDate", SqlDbType.Date, 10, ParameterDirection.Input, true, 0, 0, "BirthDate", DataRowVersion.Current, this.BirthDate));
                    sqlParams.Add(new SqlParameter("@AllowMail", SqlDbType.Bit, 1, ParameterDirection.Input, true, 0, 0, "AllowMail", DataRowVersion.Current, this.AllowMail));
                    #endregion

                    #region OUTPUT PARAMS
                    SqlParameter errorCodeParam = new SqlParameter("@ErrorCode", SqlDbType.Int, 5, ParameterDirection.ReturnValue, true, 0, 0, "ErrorCode", DataRowVersion.Current, 0);
                    SqlParameter errorMessageParam = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 1000, ParameterDirection.Output, true, 0, 0, "ErrorMessage", DataRowVersion.Current, "");
                    sqlParams.Add(errorCodeParam);
                    sqlParams.Add(errorMessageParam);
                    #endregion

                    d.ExecuteStoredNonQuery("[Account].[User_Update]", sqlParams);
                    d.Dispose();

                    ret = new BaseResponse(errorCodeParam.Value, errorMessageParam.Value);
                }
            }
            catch (Exception ex)
            {
                #region LOG EXCEPTION
                paramsLog.Add("addedParam", "I was executing stored XXX");
                Logger.Log(LogSeverity.Error, AppSettings.ApplicationName, this.ClassName, methodName, paramsLog, ex);
                #endregion

                #region MANAGE EXCEPTION
                // as needed

                ret = new BaseResponse(-1, "APPLICATION ERROR");
                #endregion
            }

            return ret;
        }

        public BaseResponse Delete()
        {
            string methodName = MethodInfo.GetCurrentMethod().Name;
            BaseResponse ret = null;
            Dictionary<string, object> paramsLog = new Dictionary<string, object>();

            try
            {
                // SqlDao method 3: implicit use, a LibParams.SqlConnectionString has already been set, and will be used for connections
                using (SqlDao d = new SqlDao())
                {
                    d.Open();

                    List<SqlParameter> sqlParams = new List<SqlParameter>();

                    #region INPUT PARAMS
                    sqlParams.Add(new SqlParameter("@AccountId", SqlDbType.VarChar, 50, ParameterDirection.Input, false, 0, 0, "AccountId", DataRowVersion.Current, this.AccountId));
                    #endregion

                    #region OUTPUT PARAMS
                    SqlParameter errorCodeParam = new SqlParameter("@ErrorCode", SqlDbType.Int, 5, ParameterDirection.ReturnValue, true, 0, 0, "ErrorCode", DataRowVersion.Current, 0);
                    SqlParameter errorMessageParam = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 1000, ParameterDirection.Output, true, 0, 0, "ErrorMessage", DataRowVersion.Current, "");
                    sqlParams.Add(errorCodeParam);
                    sqlParams.Add(errorMessageParam);
                    #endregion

                    d.ExecuteStoredNonQuery("[Account].[User_Delete]", sqlParams);
                    d.Dispose();

                    ret = new BaseResponse(errorCodeParam.Value, errorMessageParam.Value);
                }
            }
            catch (Exception ex)
            {
                #region LOG EXCEPTION
                paramsLog.Add("addedParam", "I was executing stored XXX");
                Logger.Log(LogSeverity.Error, AppSettings.ApplicationName, this.ClassName, methodName, paramsLog, ex);
                #endregion

                #region MANAGE EXCEPTION
                // as needed

                ret = new BaseResponse(-1, "APPLICATION ERROR");
                #endregion
            }

            return ret;
        }

        public BaseResponse Get()
        {
            string methodName = MethodInfo.GetCurrentMethod().Name;
            BaseResponse ret = null;
            Dictionary<string, object> paramsLog = new Dictionary<string, object>();

            try
            {
                using (SqlDao d = new SqlDao())
                {
                    d.Open();

                    List<SqlParameter> sqlParams = new List<SqlParameter>();

                    #region INPUT PARAMS
                    sqlParams.Add(new SqlParameter("@AccountId", SqlDbType.VarChar, 50, ParameterDirection.Input, false, 0, 0, "AccountId", DataRowVersion.Current, this.AccountId));
                    #endregion

                    #region OUTPUT PARAMS
                    SqlParameter errorCodeParam = new SqlParameter("@ErrorCode", SqlDbType.Int, 5, ParameterDirection.ReturnValue, true, 0, 0, "ErrorCode", DataRowVersion.Current, 0);
                    SqlParameter errorMessageParam = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 1000, ParameterDirection.Output, true, 0, 0, "ErrorMessage", DataRowVersion.Current, "");
                    sqlParams.Add(errorCodeParam);
                    sqlParams.Add(errorMessageParam);
                    #endregion

                    SqlDataReader dr = d.ExecuteStoredReader("[Account].[User_Select]", sqlParams);

                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                this.AccountId = Convert.ToInt32(dr["AccountId"]);
                                this.Name = dr["Name"].ToString();
                                this.Age = Convert.ToInt32(dr["Age"]);
                                this.BirthDate = Convert.ToDateTime(dr["BirthDate"]);
                                this.AllowMail = Convert.ToBoolean(dr["AccountId"]);
                                this.CreationDate = Convert.ToDateTime(dr["CreationDate"]);
                            }
                        }
                    }

                    dr.Close();
                    d.Dispose();

                    ret = new BaseResponse(errorCodeParam.Value, errorMessageParam.Value);
                }
            }
            catch (Exception ex)
            {
                #region LOG EXCEPTION
                paramsLog.Add("addedParam", "I was executing stored XXX");
                Logger.Log(LogSeverity.Error, AppSettings.ApplicationName, this.ClassName, methodName, paramsLog, ex);
                #endregion

                #region MANAGE EXCEPTION
                // as needed

                ret = new BaseResponse(-1, "APPLICATION ERROR");
                #endregion
            }

            return ret;
        }
    }

    public class UserResponse : BaseResponse
    {
        public User User { get; set; }

        public UserResponse(BaseResponse br) 
        {
            this.ErrorCode = br.ErrorCode;
            this.ErrorMessage = br.ErrorMessage;
        }

        public UserResponse(User u)
        {
            this.User = u;
        }

        public UserResponse(BaseResponse br, User u)
        {
            this.ErrorCode = br.ErrorCode;
            this.ErrorMessage = br.ErrorMessage;
            this.User = u;
        }
    }

    public class UsersListResponse : BaseResponse
    {
        public List<User> Users { get; set; }

        public UsersListResponse(BaseResponse br)
        {
            this.ErrorCode = br.ErrorCode;
            this.ErrorMessage = br.ErrorMessage;
        }

        public UsersListResponse(List<User> users)
        {
            this.Users = users;
        }

        public UsersListResponse(BaseResponse br, List<User> users)
        {
            this.ErrorCode = br.ErrorCode;
            this.ErrorMessage = br.ErrorMessage;
            this.Users = users;
        }
    }
}
