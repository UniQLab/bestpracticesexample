﻿using  UniqSoft.BestPracticesExample.Controllers;
using  UniqSoft.BestPracticesExample.Settings;
using UniqSoft.QFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace  UniqSoft.BestPracticesExample.Models
{
    public class MethodTemplate
    {
        public static UserResponse TestMethod(string param1, int param2)
        {
            // Set method name
            string methodName = MethodInfo.GetCurrentMethod().Name;

            // Set method retun to null (if object)
            UserResponse ur = null;

            // Logs parameters in, before processing them
            #region LOG PARAMS IN
            Dictionary<string, object> paramsLog = new Dictionary<string, object>();
            paramsLog.Add("param1", param1);
            paramsLog.Add("param2", param2);
            Logger.Log(LogSeverity.Info, AppSettings.ApplicationName, UserDMO.ClassName, methodName, paramsLog);
            #endregion

            // try catch block
            try
            {
                // do something
                // add method logic here
                // i.e.
                User u = new User();
                u.AccountId = 12;
                BaseResponse br = u.Get();

                // prepare success return object, probably with BaseResponse received from internal call
                ur = new UserResponse(br, u);
            }
            catch (Exception ex)
            {
                #region LOG EXCEPTION
                // add further parameters to paramList if needed
                paramsLog.Add("addedParam", "I was executing method XXX");
                // log exception
                Logger.LogError(AppSettings.ApplicationName, UserDMO.ClassName, methodName, paramsLog, ex);
                #endregion

                #region MANAGE EXCEPTION
                // manage exception as needed
                // i.e. MailSender.Send("EEEEEEEEERROR!!");

                // prepare error return object
                ur = new UserResponse(new BaseResponse(-1, "APPLICATION ERROR"), null);
                #endregion
            }
            finally
            {
                // when needed
                // i.e. close / dispose objects
            }

            return ur;
        }
    }
}
