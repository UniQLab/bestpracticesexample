﻿using UniqSoft.QFramework.Config;
using UniqSoft.QFramework.DAO;
using UniqSoft.QFramework.Patterns;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using  UniqSoft.BestPracticesExample.Models;
using UniqSoft.QFramework.Models;
using  UniqSoft.BestPracticesExample.Settings;

namespace  UniqSoft.BestPracticesExample.Controllers
{
    public static class UserDMO
    {
        public static string ClassName = "UserDMO";

        public static UserResponse Get(int userId)
        {
            string methodName = MethodInfo.GetCurrentMethod().Name;
            UserResponse ur = null;

            #region LOG PARAMS IN
            Dictionary<string, object> paramsLog = new Dictionary<string, object>();
            paramsLog.Add("userId", userId);
            Logger.LogInfo(AppSettings.ApplicationName, UserDMO.ClassName, methodName, paramsLog);
            #endregion

            try
            {
                User u = new User();
                u.AccountId = userId;
                BaseResponse br = u.Get();

                ur = new UserResponse(br, u);

            }
            catch (Exception ex)
            {
                #region LOG EXCEPTION
                paramsLog.Add("addedParam", "I was executing method XXX");
                Logger.LogError(AppSettings.ApplicationName, UserDMO.ClassName, methodName, paramsLog, ex);
                #endregion

                #region MANAGE EXCEPTION
                // as needed
                ur = new UserResponse(new BaseResponse(-1, "APPLICATION ERROR"), null);
                #endregion
            }

            return ur;
        }

        public static UsersListResponse GetAll()
        {
            string methodName = MethodInfo.GetCurrentMethod().Name;
            UsersListResponse ret = null;
            List<User> users = new List<User>();
            Dictionary<string, object> paramsLog = new Dictionary<string, object>();

            try
            {
                using (SqlDao d = new SqlDao())
                {
                    d.Open();

                    List<SqlParameter> sqlParams = new List<SqlParameter>();

                    #region INPUT PARAMS
                    // none
                    #endregion

                    #region OUTPUT PARAMS
                    SqlParameter errorCodeParam = new SqlParameter("@ErrorCode", SqlDbType.Int, 5, ParameterDirection.ReturnValue, true, 0, 0, "ErrorCode", DataRowVersion.Current, 0);
                    SqlParameter errorMessageParam = new SqlParameter("@ErrorMessage", SqlDbType.VarChar, 1000, ParameterDirection.Output, true, 0, 0, "ErrorMessage", DataRowVersion.Current, "");
                    sqlParams.Add(errorCodeParam);
                    sqlParams.Add(errorMessageParam);
                    #endregion

                    SqlDataReader dr = d.ExecuteStoredReader("[Account].[User_SelectAll]", sqlParams);

                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                User u = new User();

                                u.AccountId = Convert.ToInt32(dr["AccountId"]);
                                u.Name = dr["Name"].ToString();
                                u.Age = Convert.ToInt32(dr["Age"]);
                                u.BirthDate = Convert.ToDateTime(dr["BirthDate"]);
                                u.AllowMail = Convert.ToBoolean(dr["AccountId"]);
                                u.CreationDate = Convert.ToDateTime(dr["CreationDate"]);

                                users.Add(u);
                            }
                        }
                    }

                    dr.Close();
                    d.Dispose();

                    ret = new UsersListResponse(new BaseResponse(errorCodeParam.Value, errorMessageParam.Value), users);
                }
            }
            catch (Exception ex)
            {
                #region LOG EXCEPTION
                paramsLog.Add("addedParam", "I was executing stored XXX");
                Logger.LogError(AppSettings.ApplicationName, UserDMO.ClassName, methodName, paramsLog, ex);
                #endregion

                #region MANAGE EXCEPTION
                // as needed

                ret = new UsersListResponse(new BaseResponse(-1, "APPLICATION ERROR"), null);
                #endregion
            }

            return ret;
        }
    }
}
