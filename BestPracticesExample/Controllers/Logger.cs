﻿using System;
using System.Collections.Generic;

namespace   UniqSoft.BestPracticesExample.Controllers
{
    public class Logger
    {
        #region LogInfo
        public static void LogInfo(string applicationName, string className, string methodName, Dictionary<string, object> parameters)
        {
            // Call method with more parameters
            Logger.Log(LogSeverity.Info, applicationName, className, methodName, parameters, null, null);
        }
        public static void LogInfo(string applicationName, string className, string methodName, Dictionary<string, object> parameters, Exception ex)
        {
            // Call method with more parameters
            Logger.Log(LogSeverity.Info, applicationName, className, methodName, parameters, ex, null);
        }
        public static void LogInfo(string applicationName, string className, string methodName, Dictionary<string, object> parameters, string message)
        {
            // Call method with more parameters
            Logger.Log(LogSeverity.Info, applicationName, className, methodName, parameters, null, message);
        }
        public static void LogInfo(string applicationName, string className, string methodName, Dictionary<string, object> parameters, Exception ex, string message)
        {
            // Call method with more parameters
            Logger.Log(LogSeverity.Info, applicationName, className, methodName, parameters, ex, message);
        }
        #endregion

        #region LogWarning
        public static void LogWarning(string applicationName, string className, string methodName, Dictionary<string, object> parameters)
        {
            // Call method with more parameters
            Logger.Log(LogSeverity.Warning, applicationName, className, methodName, parameters, null, null);
        }
        public static void LogWarning(string applicationName, string className, string methodName, Dictionary<string, object> parameters, Exception ex)
        {
            // Call method with more parameters
            Logger.Log(LogSeverity.Warning, applicationName, className, methodName, parameters, ex, null);
        }
        public static void LogWarning(string applicationName, string className, string methodName, Dictionary<string, object> parameters, string message)
        {
            // Call method with more parameters
            Logger.Log(LogSeverity.Warning, applicationName, className, methodName, parameters, null, message);
        }
        public static void LogWarning(string applicationName, string className, string methodName, Dictionary<string, object> parameters, Exception ex, string message)
        {
            // Call method with more parameters
            Logger.Log(LogSeverity.Warning, applicationName, className, methodName, parameters, ex, message);
        }
        #endregion

        #region LogError
        public static void LogError(string applicationName, string className, string methodName, Dictionary<string, object> parameters)
        {
            // Call method with more parameters
            Logger.Log(LogSeverity.Error, applicationName, className, methodName, parameters, null, null);
        }
        public static void LogError(string applicationName, string className, string methodName, Dictionary<string, object> parameters, Exception ex)
        {
            // Call method with more parameters
            Logger.Log(LogSeverity.Error, applicationName, className, methodName, parameters, ex, null);
        }
        public static void LogError(string applicationName, string className, string methodName, Dictionary<string, object> parameters, string message)
        {
            // Call method with more parameters
            Logger.Log(LogSeverity.Error, applicationName, className, methodName, parameters, null, message);
        }
        public static void LogError(string applicationName, string className, string methodName, Dictionary<string, object> parameters, Exception ex, string message)
        {
            // Call method with more parameters
            Logger.Log(LogSeverity.Error, applicationName, className, methodName, parameters, ex, message);
        }
        #endregion

        #region Log
        public static void Log(LogSeverity severity, string applicationName, string className, string methodName, Dictionary<string, object> parameters)
        { 
            // Call method with more parameters
            Logger.Log(severity, applicationName, className, methodName, parameters, null, null);
        }
        public static void Log(LogSeverity severity, string applicationName, string className, string methodName, Dictionary<string, object> parameters, Exception ex)
        {
            // Call method with more parameters
            Logger.Log(severity, applicationName, className, methodName, parameters, ex, null);
        }
        public static void Log(LogSeverity severity, string applicationName, string className, string methodName, Dictionary<string, object> parameters, string message)
        {
            // Call method with more parameters
            Logger.Log(severity, applicationName, className, methodName, parameters, null, "");
        }
        public static void Log(LogSeverity severity, string applicationName, string className, string methodName, Dictionary<string, object> parameters, Exception ex, string message)
        {
            // Log something
            // real logic in this method
            // i.e.

            parameters.Add("ex.StackTrace", ex.StackTrace);
            parameters.Add("ex.Source", ex.Source);

            // bla bla bla
        }
        #endregion
    }

    public enum eTestEnum {
    One,
    Two,
    three
    }

    public enum LogSeverity
    {
        Info,
        Warning,
        Error
    }
}
