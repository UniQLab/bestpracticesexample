﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  UniqSoft.BestPracticesExample.Settings
{
    public class AppSettings
    {
        public const string ApplicationName = " UniqSoft.BestPracticesExample";

        public int MyInt { get; set; }
        public string MyString { get; set; }
        public DateTime MyDateTime { get; set; }

        public AppSettings()
        {
            //string myIntValue = ConfigurationManager.AppSettings["MyValue"];
            //if (!string.IsNullOrEmpty(myIntValue))
            //    this.MyInt = Convert.ToInt32(myIntValue);

            //this.MyString = ConfigurationManager.ConnectionStrings["MyConnStr"].ToString();

            //this.MyDateTime = DateTime.Now;
        }
    }
}
