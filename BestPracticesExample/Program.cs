﻿using UniqSoft.QFramework.Config;
using UniqSoft.QFramework.Patterns;
using System;
using System.Configuration;
using  UniqSoft.BestPracticesExample.Controllers;
using  UniqSoft.BestPracticesExample.Models;
using UniqSoft.QFramework.Models;

namespace  UniqSoft.BestPracticesExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Singleton<LibParams>.Instance.SqlConnectionString = ConfigurationManager.ConnectionStrings["TestConnStr"].ToString();

            #region new user definition
            User u = new User();
            u.Name = "Joe Satriani";
            u.Age = 45;
            u.BirthDate = new DateTime(1978, 06, 10);
            u.AllowMail = true;
            #endregion

            // creation
            BaseResponse br = u.Insert();
            int newId = u.AccountId;

            // clean object and retrieves user
            u = null;
            u = new User();
            u.AccountId = newId;
            br = u.Get();

            //update
            u.Name = "Steve Vai";
            br = u.Update();

            // clean object and retrieves user with DMO
            u = null;
            UserResponse ur = UserDMO.Get(newId);
            u = ur.User;

            // delete
            br = u.Delete();


            // get all
            UsersListResponse users = UserDMO.GetAll();
        }
    }
}
